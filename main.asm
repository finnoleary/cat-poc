_start:
mov rax, 1               ; sys_write
mov rdi, 1               ; stdout
mov rsi, saddr+filesize  ; start address of the appended file
mov rdx, 1               ; write one char at a time

.loop:
mov bl, byte [rsi]
cmp bl, 0
jle .end

syscall
add rsi, 1
jmp .loop

.end:
mov rax, 60
mov rdi, 0
syscall
